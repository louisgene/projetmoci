/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.telecomnancy.sensor;
import java.util.Random;
import java.util.*;
import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.SensorNotActivatedException;

/**
 *
 * @author louis
 */
public interface EtatSensor {
    public void on();
    public void off();
    public void update() throws SensorNotActivatedException;
    public double getValue() throws SensorNotActivatedException;
    public boolean getStatus();
    
    
    
    
}
