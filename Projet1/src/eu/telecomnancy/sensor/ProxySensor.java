/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.telecomnancy.sensor;
import java.util.Date;

/**
 *
 * @author louis
 */
public class ProxySensor implements ISensor {
    private ISensor capteur;
    private SimpleSensorLogger logger;
     Date date = new Date();
     LogLevel INFO;
    
    public void on() {
        capteur.on();
        logger.log(INFO, "Le" + date + " la méthode on a été appelée" );
        
        
    }
     public void off() {
        capteur.off();
        logger.log(INFO, "Le" + date + " la méthode off a été appelée" );
        
        
    }
 public boolean getStatus() {
       
        logger.log(INFO, "Le" + date + " la méthode getStatus a été appelée et a retourné" + capteur.getStatus() );
        return capteur.getStatus();
        
    }
 public void update() throws SensorNotActivatedException {
     capteur.update();
     logger.log(INFO, "Le" + date + " la méthode update a été appelée" );
 }
 public double getValue() throws SensorNotActivatedException {
        logger.log(INFO, "Le" + date + " la méthode getStatus a été appelée et a retourné" + capteur.getValue() );
        return capteur.getValue();
 }
     public void setSensor(ISensor capteur1) {
        this.capteur = capteur1;
    }
     public void setLogger( SimpleSensorLogger log ){
         this.logger=log;
     }
}
