/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.telecomnancy.sensor;

/**
 *
 * @author louis
 */
public class CommandGetValue implements Command {

    private ISensor capteur1;

    public void setOn(ISensor capteur) {
        this.capteur1 = capteur;
    }

    public void execute() {
        try {
            capteur1.getValue();
        } catch (SensorNotActivatedException sensorNotActivatedException) {
            sensorNotActivatedException.printStackTrace();
        }
    }
}
