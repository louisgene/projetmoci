/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eu.telecomnancy.sensor;

/**
 *
 * @author louis
 */
public class DecorateurArrondi implements ISensor {
    
    private ISensor capteur;
    public void on() {
        capteur.on();
    }
    public void off() {
        capteur.off();
    }
    public boolean getStatus() {
    return capteur.getStatus();
}
    public void update() throws SensorNotActivatedException {
    capteur.update();
}
    public double getValue() throws SensorNotActivatedException {
        
        int temperature=0 ;
        temperature = (int)capteur.getValue();
        temperature = 9/5*temperature +32 ;
        return temperature;
    }
    public void setSensor( ISensor capteur) {
        this.capteur = capteur;
    }
}
