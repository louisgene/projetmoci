/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.telecomnancy.sensor;

/**
 *
 * @author louis
 */
public class Invoqueur {
    private Command CommandOn;
    private Command CommandOff;
    private Command CommandUpdate;
    private Command CommandGetStatus;
    private Command CommandGetValue;
    
    public void invoquerCommandeOn() {
        if (CommandOn!= null) {
                CommandOn.execute();
        }
    }
    public void invoquerCommandOff() {
        if (CommandOff!= null) {
                CommandOff.execute();
        }
    }
    public void invoquerCommandUpdate() {
        if (CommandUpdate!= null) {
                CommandUpdate.execute();
        }
    }
    public void invoquerCommandGetStatus() {
        if (CommandGetStatus!= null) {
               CommandGetStatus.execute();
        }
    }
    public void invoquerCommandGetValue() {
        if (CommandGetValue!= null) {
                CommandGetValue.execute();
        }
    }
    public void setCommandOn(Command commande) {
        CommandOn = commande;
    }
     public void setCommandOff(Command commande) {
        CommandOff = commande;
    }
      public void setCommandUpdate(Command commande) {
        CommandUpdate = commande;
    }
       public void setCommandGetValue(Command commande) {
        CommandGetValue = commande;
    }
        public void setCommandGetStatus(Command commande) {
        CommandGetStatus = commande;
    }
}
