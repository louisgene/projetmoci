/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.telecomnancy.sensor;

import java.util.Random;
import eu.telecomnancy.sensor.ISensor;

/**
 *
 * @author genetier1u
 */
public  class SensorAdaptateur implements ISensor {

    public LegacyTemperatureSensor lts = new LegacyTemperatureSensor();

    public void on() {
        if (lts.getStatus()) {

        } else {
            lts.onOff();
        }

    }

    public void off() {
        if (!lts.getStatus()) {

        } else {
            lts.onOff();
        }

    }

    public boolean getStatus() {
        return lts.getStatus();
    }
    
    public double getValue() {
        return lts.getTemperature();
    }

    public void update() throws SensorNotActivatedException {
        if (!lts.getStatus()){
            throw (new SensorNotActivatedException(""));
        }
    }

}
