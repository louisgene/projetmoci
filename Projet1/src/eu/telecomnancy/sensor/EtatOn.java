/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.telecomnancy.sensor;

import java.util.Random;

/**
 *
 * @author louis
 */
public class EtatOn implements EtatSensor{
    private boolean status;
    private double value;
    
    public EtatOn() {
    this.status=true;
    this.value = 0;
}
    public void on() {
}
      public void off() {
    this.status = false ;
}
    public void update() {
        value = (new Random()).nextDouble() * 100;
    }
    public double getValue(){
        return value;
    }
    public boolean getStatus(){
        return status;
    }
}
