/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.telecomnancy.sensor;

import java.util.Random;

/**
 *
 * @author louis
 */
public class EtatOff implements EtatSensor {
     private boolean status;
    private double value;
    
    public EtatOff() {
    this.status=false;
    this.value = 0;
}
    public void on() {
        this.status= true;
}
      public void off() {

}
    public void update() throws SensorNotActivatedException{
        throw new SensorNotActivatedException("Sensor must be activated before acquiring new values.");
    }
    public double getValue() throws SensorNotActivatedException{
          throw new SensorNotActivatedException("Sensor must be activated to get its value.");
          
    }
      public boolean getStatus(){
        return status;
    }
}
    

