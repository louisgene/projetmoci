/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.telecomnancy.sensor;

import java.util.Properties;
import telecomnancy.helpers.ReadPropertyFile;
import java.io.IOException;

/**
 *
 * @author louis
 */
public class FactorySensor {

    public static ISensor makeSensor() {
        ISensor capteur;
        capteur = null;
        ReadPropertyFile lecture = new ReadPropertyFile();
        Properties propriété = null;
        try {
            propriété = lecture.readFile("/eu/telecomnancy/app.properties");
        } catch (IOException e) {
            e.printStackTrace();

        }
        if ("TemperatureSensor".equals(propriété)) {
            capteur = new TemperatureSensor();

        }
        if ("newTemperatureSensor".equals(propriété)) {
            capteur = new newTemperatureSensor();

        }
        if ("LegacyTemperatureSensor".equals(propriété)) {
            capteur = new SensorAdaptateur();

        }
        if ("ProxySensor".equals(propriété)) {
            capteur = new ProxySensor();

        }
        else {
            capteur = new TemperatureSensor();
        }

        return capteur;
    }
}
