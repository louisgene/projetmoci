/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.telecomnancy.sensor;

import java.util.Random;
import java.util.*;
import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.SensorNotActivatedException;

/**
 *
 * @author louis
 */
public class DecorateurFahrenheit implements ISensor {

    private ISensor capteur;

    public void on() {
        capteur.on();
    }

    public void off() {
        capteur.off();
    }

    public boolean getStatus() {
        return capteur.getStatus();
    }

    public void update() throws SensorNotActivatedException {
        capteur.update();
    }

    public double getValue() throws SensorNotActivatedException {

        double temperature = 0;
        temperature = capteur.getValue();
        temperature = 9 / 5 * temperature + 32;
        return temperature;
    }

    public void setSensor(ISensor capteur1) {
        this.capteur = capteur1;
    }
}
