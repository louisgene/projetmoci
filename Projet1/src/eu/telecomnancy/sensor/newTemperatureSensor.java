package eu.telecomnancy.sensor;

import java.util.Random;
import java.util.*;
import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.SensorNotActivatedException;

public class newTemperatureSensor  implements ISensor {
    private EtatSensor state;
   

    @Override
    public void on() {
        state.on();
    }

    @Override
    public void off() {
        state.off();
    }

    @Override
    public boolean getStatus() {
        return state.getStatus();
    }

    @Override
    public void update() throws SensorNotActivatedException {
       state.update();
          
    }

    @Override
    public double getValue() throws SensorNotActivatedException {
        return state.getValue();
       
    }

}
