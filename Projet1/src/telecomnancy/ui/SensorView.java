package telecomnancy.ui;

import eu.telecomnancy.sensor.DecorateurFahrenheit;
import eu.telecomnancy.sensor.SensorNotActivatedException;
import eu.telecomnancy.sensor.DecorateurArrondi;
import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.Invoqueur;
import eu.telecomnancy.sensor.CommandGetStatus;
import eu.telecomnancy.sensor.CommandGetValue;
import eu.telecomnancy.sensor.CommandOn;
import eu.telecomnancy.sensor.CommandOff;
import eu.telecomnancy.sensor.CommandUpdate;

import javax.swing.*;
import java.awt.*;
import java.util.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


public class SensorView extends JPanel implements Observer {

    private ISensor sensor;
    

    public JLabel value = new JLabel("N/A °C");
    private JButton on = new JButton("On");
    private JButton off = new JButton("Off");
    private JButton update = new JButton("Acquire");
    private JButton Fahrenheit = new JButton("Fahrenheit");
    private JButton Celsius = new JButton("Celsius");
    private JButton FahrenheitA = new JButton("Fahrenheit Arrondi");
    private Invoqueur inv = new Invoqueur();    

    public SensorView(ISensor c) {
        this.sensor = c;
        CommandOn CommandeOn = new CommandOn();
        CommandOff CommandeOff = new CommandOff();
        CommandUpdate CommandeUpdate = new CommandUpdate();
        CommandeOn.setOn(sensor);
        CommandeUpdate.setOn(sensor);
        CommandeOff.setOn(sensor);
        inv.setCommandOff(CommandeOff);
        inv.setCommandOn(CommandeOn);
        inv.setCommandUpdate(CommandeUpdate);
        

        ((Observable) this.sensor).addObserver(this);
        this.setLayout(new BorderLayout());

        value.setHorizontalAlignment(SwingConstants.CENTER);
        Font sensorValueFont = new Font("Sans Serif", Font.BOLD, 18);
        value.setFont(sensorValueFont);

        this.add(value, BorderLayout.CENTER);

        on.addActionListener(new ActionListener() {
            
            @Override
            public void actionPerformed(ActionEvent e) {
                inv.invoquerCommandeOn();
            }
        });

        off.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                inv.invoquerCommandOff();
            }
        });

        update.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                    inv.invoquerCommandUpdate();                
            }
        });

        Celsius.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                try {
                    value.setText("" + sensor.getValue());
                } catch (SensorNotActivatedException sensorNotActivatedException) {
                    sensorNotActivatedException.printStackTrace();
                }
            }
        });
        Fahrenheit.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                DecorateurFahrenheit capteur1 = new DecorateurFahrenheit();
                capteur1.setSensor(c);

                try {
                    value.setText("" + capteur1.getValue());
                } catch (SensorNotActivatedException sensorNotActivatedException) {
                    sensorNotActivatedException.printStackTrace();
                }
            }
        });
        FahrenheitA.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                DecorateurArrondi capteur2 = new DecorateurArrondi();
                capteur2.setSensor(c);

                try {
                    value.setText("" + capteur2.getValue());
                } catch (SensorNotActivatedException sensorNotActivatedException) {
                    sensorNotActivatedException.printStackTrace();
                }
            }
        });

        JPanel buttonsPanel = new JPanel();
        buttonsPanel.setLayout(new GridLayout(2, 3));
        buttonsPanel.add(update);
        buttonsPanel.add(on);
        buttonsPanel.add(off);
        buttonsPanel.add(Celsius);
        buttonsPanel.add(Fahrenheit);
        buttonsPanel.add(FahrenheitA);

        this.add(buttonsPanel, BorderLayout.SOUTH);
    }

    public void update(Observable obs, Object arg) {
        try {
            this.value.setText("" + this.sensor.getValue());
        } catch (SensorNotActivatedException e) {
        }
    }
}
